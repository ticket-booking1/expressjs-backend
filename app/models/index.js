const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  port: dbConfig.port,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

// const dbConfig = require('../config/db.config');
// const Sequelize = require('sequelize');
// const sequelize = new Sequelize(
//     dbConfig.DB, 
//     dbConfig.USER, 
//     dbConfig.PASSWORD, {
//         host: dbConfig.HOST,
//         dialect: dbConfig.dialect,
//         operatorAlias: false,
//         pool: {
//             max: dbConfig.pool.max,
//             min: dbConfig.pool.min,
//             acquire: dbConfig.pool.acquire,
//             idle: dbConfig.pool.idle
//         },
//     });
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

// define semua models yang ada pada aplikasi
db.role = require('./role.model')(sequelize, Sequelize);
db.user = require('./user.model')(sequelize, Sequelize);
db.concerts = require('./concert.model')(sequelize, Sequelize);
db.orders = require('./order.model')(sequelize, Sequelize);

db.orders.belongsTo(db.concerts,{
    foreignKey: 'concert_id',
  });

db.role.belongsToMany(db.user, {
    through: 'user_roles',
    foreygnKey: 'roleId',
    otherKey: 'userId'
});
db.user.belongsToMany(db.role, {
    through: 'user_roles',
    foreygnKey: 'userId',
    otherKey: 'roleId'
});

// db.orders.belongsTo(db.concerts,{
//     foreignKey: 'concert_id',
//   })

db.ROLES = ['user', 'admin', "moderator"];

module.exports = db;