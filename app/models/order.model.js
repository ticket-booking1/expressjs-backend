module.exports = (sequelize, Sequelize) => {
    const Order = sequelize.define('order', {
        user_id: {
            type: Sequelize.INTEGER,
        },
        concert_id: {
            type: Sequelize.INTEGER,
        },
        qty: {
            type: Sequelize.INTEGER,
        }
    });
    return Order;
}