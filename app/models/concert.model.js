module.exports = (sequelize, Sequelize) => {
    const Concert = sequelize.define('concert', {
        image: {
            type: Sequelize.STRING,
        },
        title: {
            type: Sequelize.STRING,
        },
        description: {
            type: Sequelize.TEXT,
        },
        price: {
            type: Sequelize.INTEGER,
        },
        qty: {
            type: Sequelize.INTEGER,
        },
        available: {
            type: Sequelize.INTEGER
        }
    });
    return Concert;
}