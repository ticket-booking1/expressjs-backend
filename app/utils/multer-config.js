const multer = require("multer")
const storageEngine = multer.diskStorage({
    destination: (req, file, cb) => { cb(null, 'resource/uploads') },
    filename: (req, file, cb) => { cb(null, file.originalname) }, })

const imageFilter = (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpeg" || file.mimetype == "image/jpg") {
    cb(null, true) } else { cb(null, false)}
    }
exports.imageUploadHandler = multer({ storage: storageEngine, fileFilter: imageFilter })