const concertController = require('../controllers/concert.controller');
const { authJwt } = require('../middleware/auth');
const { imageUploadHandler } = require("../utils/multer-config.js")



const router = require('express').Router();

router.post("/", imageUploadHandler.fields([{ name: "image", maxCount: 1 }]), concertController.create)
router.post("/:id", imageUploadHandler.fields([{ name: "image", maxCount: 1 }]), concertController.update)


// router.post('/', concertController.create);
// router.get('/', [authJwt.verifyToken], concertController.findAll);
router.get('/available', concertController.findAvailable);
router.get('/', concertController.findAll);
router.put('/:id', concertController.update);
router.put('/:id', concertController.update);
router.delete('/:id', concertController.delete);
router.get('/:id', concertController.findOne);

module.exports = router;