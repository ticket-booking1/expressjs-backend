const orderController = require('../controllers/order.controller');
const router = require('express').Router();
const { authJwt } = require('../middleware/auth');

router.post('/',[authJwt.verifyToken], orderController.create);
router.get('/',[authJwt.verifyToken],orderController.findAll);
router.put('/:id', orderController.update);
router.put('/:id', orderController.update);
router.delete('/:id', orderController.delete);
router.get('/:id', orderController.findOne);
// router.get('/', [authJwt.verifyToken], orderController.findAll);

module.exports = router;