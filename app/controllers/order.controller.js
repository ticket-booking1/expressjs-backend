const db = require("../models");
// const { Op, sequelize } = require("sequelize");
const Op = db.Sequelize.Op;
const sequelize = db.sequelize;
const Order = db.orders;
const Concert = db.concerts;

// create a order
exports.create = async(req, res) => {
  // validate request
  if (!req.body.concert_id>0) {
    return res.status(400).send({
      message: "Concert can not be empty",
    });
  }
  if (!req.body.qty>0) {
    return res.status(400).send({
      message: "Quantity can not be empty",
    });
  }
  
  // create a order
  const order = {
    concert_id: req.body.concert_id,
    qty: req.body.qty,
    user_id: req.userId
  };

  // save order in the database
  // const t = await sequelize.transaction();
  try {
    const result = await sequelize.transaction(async (t) => {

      // check actually available ticket
      const concertID = req.body.concert_id
      // check ticket available
      const concert  = await Concert.findOne({ where: { id: req.body.concert_id, available: { [Op.gte]: req.body.qty } } }, { transaction: t });
      // create order
      if (concert == null) {
        throw new Error('Ticket Not Available');
        return
      }
      const rOrder = await Order.create(order, { transaction: t });
      // concert.available -= req.body.qty;
      // update master of available ticket
      const updateData ={
        available: concert.available - req.body.qty
      }
      
      await Concert.update(updateData, {
        where: { id: concertID },
      }, { transaction: t });
      //commit
      // return rOrder
      res.json({
        message: "Order created successfully.",
        data: rOrder,
      });
    });
  } catch (error) {
    res.status(500).json({
      message: error.message || "Some error occurred while creating the Order.",
      data: null,
    });
    // If the execution reaches this line, an error was thrown.
    // We rollback the transaction.
    // await t.rollback();
  
  }
};

// retrieve all orders
exports.findAll = (req, res) => {
  // Order.belongsTo(Concert, {
  //   foreignKey: 'concert_id',
  // });
  
  Order.findAll(
    // {
    //   where: { user_id: req.userId },
    // },
    { include: Concert,
      where: { user_id: req.userId }, 
    },
  )
    .then((orders) => {
      res.json({
        message: "Orders retrieved successfully.",
        data: orders,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while retrieving orders.",
        data: null,
      });
    });
};

// update
exports.update = (req, res) => {
  const id = req.params.id;
  Order.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.json({
          message: "Order updated successfully.",
          data: req.body,
        });
      } else {
        res.json({
          message: `Cannot update order with id=${id}. Maybe order was not found or req.body is empty!`,
          data: req.body,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while updating the order.",
        data: null,
      });
    });
};

// delete
exports.delete = (req, res) => {
  const id = req.params.id;
  Order.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.json({
          message: "Order deleted successfully.",
          data: req.body,
        });
      } else {
        res.json({
          message: `Cannot delete order with id=${id}. Maybe order was not found!`,
          data: req.body,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while deleting the order.",
        data: null,
      });
    });
};

// find by id
exports.findOne = (req, res) => {
  Order.findByPk(req.params.id)
    .then((order) => {
      res.json({
        message: "Order retrieved successfully.",
        data: order,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while retrieving order.",
        data: null,
      });
    });
};
