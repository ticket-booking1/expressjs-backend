const db = require("../models");
const Concert = db.concerts;
const { Op } = require("sequelize");
// create a concert
exports.create = (req, res) => {
  // validate request
  if (!req.body.title) {
    return res.status(400).send({
      message: "Title can not be empty",
    });
  }

  // create a concert
  const concert = {
    title: req.body.title,
    description: req.body.description,
    price: req.body.price,
    qty: req.body.qty,
    available: req.body.available,
    image: req.files.image[0].filename
  };

  // save concert in the database
  Concert.create(concert)
    .then((data) => {
      res.json({
        message: "Concert created successfully.",
        data: data,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while creating the Concert.",
        data: null,
      });
    });
};

// retrieve all concerts
exports.findAll = (req, res) => {
  Concert.findAll()
    .then((concerts) => {
      res.json({
        message: "Concerts retrieved successfully.",
        data: concerts,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while retrieving concerts.",
        data: null,
      });
    });
};

exports.findAvailable = (req, res) => {
  Concert.findAll({
    where: {
      available: {
        [Op.gt]: 0
      }
    }
  })
    .then((concerts) => {
      res.json({
        message: "Concerts retrieved successfully.",
        data: concerts,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while retrieving concerts.",
        data: null,
      });
    });
};

// update
exports.update = (req, res) => {
  const id = req.params.id;
  if (req.files.image != undefined) {
    req.body.image =  req.files.image[0].filename;
  }
  Concert.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.json({
          message: "Concert updated successfully.",
          data: req.body,
        });
      } else {
        res.json({
          message: `Cannot update concert with id=${id}. Maybe concert was not found or req.body is empty!`,
          data: req.body,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while updating the concert.",
        data: null,
      });
    });
};

// delete
exports.delete = (req, res) => {
  const id = req.params.id;
  Concert.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.json({
          message: "Concert deleted successfully.",
          data: req.body,
        });
      } else {
        res.json({
          message: `Cannot delete concert with id=${id}. Maybe concert was not found!`,
          data: req.body,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while deleting the concert.",
        data: null,
      });
    });
};

// find by id
exports.findOne = (req, res) => {
  Concert.findByPk(req.params.id)
    .then((concert) => {
      res.json({
        message: "Concert retrieved successfully.",
        data: concert,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || "Some error occurred while retrieving concert.",
        data: null,
      });
    });
};
