FROM node:14

WORKDIR /puguh-app
COPY package.json .
RUN npm install
COPY . .
CMD npm start