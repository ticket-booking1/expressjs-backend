const express = require('express');
const cors = require('cors');
const app = express();
const port = 4000;
const authRoute = require('./app/routes/auth.routes')
const concertRoute = require('./app/routes/concert.routes')
const orderRoute = require('./app/routes/order.routes')

require("dotenv").config();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use(express.static('resource/uploads'));

const db = require('./app/models');
const { use } = require('express/lib/application');
const Role = db.role;
db.sequelize.sync();
// db.sequelize.sync({forgive: true})
// .then(() => {
//     console.log('Database berhasil di sync');
//     createRoles()
// })

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.use('/api/auth', authRoute)
app.use('/api/concerts',concertRoute)
app.use('/api/orders', orderRoute)
app.use('/images', express.static('resource/uploads'));

// app.listen(port, () => console.log(`App listening on port http://localhost:${port}!`));
const PORT = process.env.NODE_DOCKER_PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

function createRoles() {
    Role.create({
        id: 1,
        name: 'user'
    })
    Role.create({
        id: 2,
        name: 'moderator'
    })
    Role.create({
        id: 1,
        name: 'admin'
    })
}